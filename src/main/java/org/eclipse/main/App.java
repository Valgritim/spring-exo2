package org.eclipse.main;

import org.eclipse.nation.European;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    	European e = (European) context.getBean("GB");
    	e.saluer();
    }
}
